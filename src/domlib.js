function nodeFactory(thisDocument, nodeName, nodeProps, ...childNodes) {
  if (!thisDocument || nodeName == null) {
    return null;
  }
  const node = thisDocument.createElement(nodeName);
  if (nodeProps !== undefined) {
    if (typeof nodeProps === 'object' && nodeProps.constructor.name === 'Object') {
      for (const [prop, value] of Object.entries(nodeProps)) {
        node.setAttribute(prop, value);
      }
    } else {
      childNodes.unshift(nodeProps);
    }
  }
  for (const child of childNodes) {
    if (typeof child === 'string' || typeof child === 'number') {
      node.appendChild(thisDocument.createTextNode(child));
    } else if (child != null) {
      node.appendChild(child);
    }
  }
  return node;
}

function partial(func, ...fixedParams) {
  return function (...lateParams) {
    return func(...fixedParams, ...lateParams);
  };
}

/**
 * Simple DOM factory helper. Usage:
 *
 * ```
 * const div = H.div(
 *   H.h1('Hello'),
 *   H.a({href: 'https://www.duckduckgo.com'}, 'duck duck go!'),
 *   H.ul(
 *     ...['item1', 'item2', 'item3'].map(label => H.li(label))
 *   )
 * )
 *)
 * ```
 */
function dombuilder(thisDocument) {
    return new Proxy(
        {},
        {
          get: function (obj, prop) {
            return prop in obj ? obj[prop] : partial(nodeFactory, thisDocument, prop);
          },
        },
      );
}
