/**
 * This script will be injected in the inspected page.
 *
 * A `__toucanspy__` identifier will be injected on `window`.
 */
window.__toucanspy__ = (function () {
  /**
   * replace circular reference with hardcoded string,
   * cf. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value#Examples
   */
  function getCircularReplacer() {
    const seen = new WeakSet();
    return (key, value) => {
      if (typeof value === 'object' && value !== null) {
        if (seen.has(value)) {
          return '<circular-ref>';
        }
        seen.add(value);
      }
      return value;
    };
  }

  function getVuexState() {
    for (const elt of document.querySelectorAll('*')) {
      if (elt.__vue__ && elt.__vue__.$store) {
        return elt.__vue__.$store.state;
      }
    }
    return null;
  }

  /**
   * little helper to convert Object-compatible objects to plain objects
   * and / or make a deep copy
   */
  function asPlainObject(obj) {
    return JSON.parse(JSON.stringify(obj, getCircularReplacer()));
  }

  /**
   * return current Vuex state
   * XXX what if multiple modules / namespaces
   */
  function getToucanDebugData() {
    // make a copy of state to avoid mutating the original one
    const state = asPlainObject(getVuexState());
    if (state && state.locales && state.locales.polyglot) {
      delete state.locales.polyglot;
    }
    return asPlainObject({
      state,
      localStorage: window.localStorage,
      __tctc__: window.__tctc__,
    });
  }

  // Put here everything that you'll need exposed under `__toucanspy__`
  return {
    getToucanDebugData,
  };
})();

window.setTimeout(() => {
  const data = window.__toucanspy__.getToucanDebugData();
  window.postMessage({ source: 'toucanspy', type: 'SET_STATE', data }, '*');
});
