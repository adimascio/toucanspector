let thisWindow;
let thisDocument;
let H; // dom builder

const LOGS_LIMIT = 30;
let API_BASEROUTE;

/**
 *
 * @param {string} jsonString the jsons string to parse
 * @param {object} fallback what should be returned if any error occurs during parsing
 */
function safeParse(jsonString, fallback = { errmsg: 'failed to parse JSON' }) {
  try {
    return JSON.parse(jsonString);
  } catch (e) {
    return fallback;
  }
}

/**
 * Simple implementation of lodash.get
 *
 * `safeGet({}, 'a')` will return `undefined`
 * `safeGet({a: 1}, 'a')` will return `1`
 * `safeGet({a: {b: {c: 1}}}, 'a.b.c')` will return `1`
 * `safeGet({a: {b: {c: 1}}}, 'a.b.c.d')` will return `undefined`
 *
 */
function safeGet(obj, path, fallback = undefined) {
  chunks = path.split('.');
  for (const propname of path.split('.')) {
    if (obj[propname] === undefined) {
      return fallback;
    }
    obj = obj[propname];
  }
  return obj;
}

/**
 * remove a list of properties from an object and return the new object
 * (sort of like _.omit)
 *
 * @param {object} obj the object to remove the properties from
 * @param  {string[]} keys the list of property names to remove
 */
function omit(obj, ...keys) {
  const newObj = {};
  for (const key of Object.keys(obj)) {
    if (!keys.includes(key)) {
      newObj[key] = obj[key];
    }
  }
  return newObj;
}

/**
 * Replace the content of a tab (e.g. `datarequesters`, `logs`) with a new DOM node
 *
 * @param {string} tabId the id of the tab in `panel.html` to replace the content of
 * @param {Node} newContent the DOM node to insert
 */
function replaceTabContent(tabId, newContent) {
  const tabContent = thisDocument.querySelector(`#${tabId}Tab`);
  tabContent.replaceWith(H.div({ id: `${tabId}Tab`, class: 'tab-content' }, newContent));
}

/**
 * Compute et reset logs tab content
 *
 * @param {string[][]} logs Tucana's `__tctc__.logs`-like structure
 */
function setLogs(logs) {
  replaceTabContent(
    'logs',
    H.table(
      H.thead(H.tr(H.th('logger'), H.th('message'))),
      H.tbody(
        ...logs
          .slice(0, LOGS_LIMIT)
          .map(([logger, ...chunks]) => H.tr(H.td(logger), H.td(chunks.join(' ')))),
      ),
    ),
  );
}

/**
 * Compute et reset userfinfos tab content
 *
 * @param {object} userInfos Tucana's `state.application.userInfos`-like structure
 */
function setUserInformations(userInfos) {
  const newContent = H.pre(H.code({ class: 'json' }, JSON.stringify(userInfos, null, 2)));
  replaceTabContent('userinfos', newContent);
  hljs.highlightBlock(newContent);
}

/**
 * return `true` if the request is a data request (i.e. a POST request on `/data?`)
 *
 * @param {object} req the request being tested
 */
function isDataRequest(req) {
  return req.method === 'POST' && req.url.indexOf('/data') !== -1;
}

/**
 * Append a request line in the `datarequests` table
 *
 * @param {object} req the request
 */
function appendDataRequest(req) {
  const tbody = thisDocument.querySelector(`#datarequestsTab table tbody`);
  let timeClass = 'ok';
  if (req.totalTime >= 400 && req.totalTime <= 800) {
    timeClass = 'warn';
  } else if (req.totalTime > 800) {
    timeClass = 'danger';
  }
  const explain = safeGet(req, 'responseBody.meta.explain');
  const formattedExplain = explain ? formatExplainInfos(explainInfos(explain[0])) : '';
  const newLine = H.tr(
    H.td(H.pre(H.code({ class: 'json' }, JSON.stringify(req.requestBody, null, 2)))),
    H.td({ class: `r${req.statusCode}` }, req.statusCode),
    H.td({ class: timeClass }, Math.round(req.totalTime)),
    H.td(req.responseSize),
    H.td(req.responseBody && req.responseBody.meta ? req.responseBody.meta.count : 0),
    H.td(H.pre(H.code({ class: 'json' }, JSON.stringify(req.responseBody, null, 2)))),
    H.td(formattedExplain),
  );
  tbody.appendChild(newLine);
  for (const block of newLine.querySelectorAll('pre')) {
    hljs.highlightBlock(block);
  }
}

/**
 * Append the list of server logs emitted during the request
 *
 * @param {object} req the request
 */
function appendServerLogs(req) {
  const logs = safeGet(req, 'responseBody.meta.logs');
  if (logs) {
    thisDocument.querySelector('#serverlogsTab').appendChild(H.pre(logs));
  }
}

/**
 * Compute et reset requesters tab content
 *
 * @param {object} state Tucana's `state`-like structure
 */
function setRequesters(state) {
  const requestersState = {};
  for (const [key, value] of Object.entries(safeGet(state, 'smallApp.requesters.__values__'), {})) {
    if (typeof value === 'object') {
      requestersState[key] = omit(value, '__children__');
    } else {
      requestersState[key] = value;
    }
  }
  const newContent = H.pre(H.code({ class: 'json' }, JSON.stringify(requestersState, null, 2)));
  replaceTabContent('requesters', newContent);
  hljs.highlightBlock(newContent);
}

function updatePanelInfos({ state, __tctc__ }) {
  API_BASEROUTE = state.application.apiBaseRoute;
  setLogs(__tctc__.logs);
  setRequesters(state);
  setUserInformations(safeGet(state, 'application.userInfos'));
}

function onPanelLoaded(panelWindow) {
  thisWindow = panelWindow;
  thisDocument = panelWindow.document;
  H = dombuilder(thisDocument);
  chrome.devtools.inspectedWindow.eval('window.__toucanspy__.getToucanDebugData()', function (
    result,
    isException,
  ) {
    if (isException) {
      console.warn('got exception', isException);
    } else {
      updatePanelInfos(result);
    }
  });
}

chrome.devtools.panels.create('Toucan', '/assets/icon128.png', '/src/panel.html', function (panel) {
  panel.onShown.addListener(onPanelLoaded);
});

/**
 * When a request is finished, consolidate request statistics (status, total time, etc.)
 * then if it's a data request, fetch its body and append statistics in the requests panel.
 *
 * @param {object} request the request being finished, (cf.
 * http://www.softwareishard.com/blog/har-12-spec/ for its API)
 */
chrome.devtools.network.onRequestFinished.addListener(function (request) {
  const req = {
    method: request.request.method,
    url: request.request.url,
    requestBody: request.request.postData ? safeParse(request.request.postData.text) : {},
    statusCode: request.response.status,
    totalTime:
      request.timings.blocked +
      request.timings.dns +
      request.timings.connect +
      request.timings.send +
      request.timings.wait +
      request.timings.receive,
    responseSize: request.response.headersSize + request.response.bodySize,
    responseBody: null,
  };
  // If it's a data request, fetch its body and append a new line in the
  // requests table
  if (isDataRequest(req)) {
    request.getContent((body) => {
      const response = safeParse(body || '{}');
      if (response.data !== undefined) {
        response.data = response.data.slice(0, 2);
      }
      req.responseBody = response;
      if (thisDocument) {
        appendDataRequest(req);
        appendServerLogs(req);
      }
      // send back body to any other extension part if needed
      if (request.request) {
        chrome.runtime.sendMessage({
          response: body,
        });
      }
    });
  }
});
