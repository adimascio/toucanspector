/**
 * background script
 */

/**
 * add `X-TOUCANSPECTOR` extra headers
 */
chrome.webRequest.onBeforeSendHeaders.addListener(
  function ({ requestHeaders }) {
    requestHeaders.push({ name: 'X-TOUCANSPECTOR', value: 'on' });
    return { requestHeaders };
  },
  { urls: ['<all_urls>'] },
  ['blocking', 'requestHeaders'],
);
