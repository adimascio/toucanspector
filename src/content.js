/**
 * content script
 */
(function implantSpy() {
  console.log('spy', chrome.extension.getURL('src/toucanspy.js'));
  const script_ = document.createElement('script');
  script_.src = chrome.extension.getURL('src/toucanspy.js');
  document.head.appendChild(script_);
  script_.onload = function () {
    script_.parentNode.removeChild(script_);
  };
})();

window.addEventListener('message', function (event) {
  // only accept messages from the same frame
  if (event.source !== window) {
    return;
  }

  var message = event.data;

  // only accept messages that we know are ours
  if (typeof message !== 'object' || message === null || !message.source === 'toucanspy') {
    return;
  }

  // pass it back to background page
  chrome.runtime.sendMessage(message);
});
