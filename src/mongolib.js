function getStageInfos(stage, inputlevel = 0) {
  const infos = [];
  if (stage.inputStage !== undefined) {
    infos.push(...getStageInfos(stage.inputStage, inputlevel + 1));
  } else if (stage.inputStages !== undefined) {
    infos.push(...stage.inputStages.map((inputStage) => getStageInfos(inputStage, inputlevel + 1)));
  }
  infos.push({
    name: stage.stage,
    inputlevel,
    filter: stage.filter,
    indexName: stage.indexName,
    stageTime: stage.executionTimeMillisEstimate,
    totalTime: stage.executionTimeMillis,
    nbKeys: stage.keysExamined,
    nbDocs: stage.totalDocsExamined,
    nReturned: stage.nReturned,
  });
  return infos;
}

function explainInfos(explainPlan) {
  const stageInfos = [];
  for (const stagedef of explainPlan.stages) {
    const stageName = Object.keys(stagedef)[0];
    if (stageName === '$cursor') {
      const executionStats = stagedef.$cursor.executionStats;
      stageInfos.push({
        name: '$cursor',
        substages: getStageInfos(executionStats.executionStages),
        nReturned: executionStats.nReturned,
        stageTime: executionStats.executionTimeMillis,
        totalKeysExamined: executionStats.totalKeysExamined,
        totalDocsExamined: executionStats.totalDocsExamined,
      });
    } else {
      stageInfos.push({
        name: stageName,
        inputlevel: 0,
      });
    }
  }
  return stageInfos;
}

function _extract_fetched_fields(filter) {
  const fields = [];
  for (const [key, value] of Object.entries(filter)) {
    if (key === '$and' || key === '$or') {
      fields.push(..._extract_fetched_fields(value));
    } else if (typeof value === 'objet' && value.constructor.name === 'Array') {
      for (const restriction of value) {
        fields.push(..._extract_fetched_fields(value));
      }
    } else {
      fields.push(...Object.keys(value));
    }
  }
  return fields;
}

function _formatStageInfos(stageInfos) {
  const chunks = [];
  if (stageInfos.name === 'IXSCAN') {
    chunks.push(`idx: ${stageInfos.indexName}`);
  } else if (stageInfos.name === 'FETCH' && stageInfos.filter) {
    const unindexed = _extract_fetched_fields(stageInfos.filter);
    chunks.push(`unindexed: ${unindexed.join(', ')}`);
  }
  if (stageInfos.stageTime) {
    chunks.push(`${stageInfos.stageTime} ms`);
  }
  if (stageInfos.nReturned) {
    chunks.push(`#records = ${stageInfos.nReturned}`);
  }
  const formattedChunks = chunks.join(', ');
  const div = H.div({ class: 'explain' }, H.span({ class: 'stageName' }, stageInfos.name));
  if (formattedChunks) {
    div.appendChild(H.span({ class: 'details' }, ` (${formattedChunks})`));
  }
  return div;
}

function formatExplainInfos(infos) {
  const lis = [];
  for (const stageInfos of infos) {
    const children = [_formatStageInfos(stageInfos)];
    if (stageInfos.substages) {
      children.push(
        H.ul(...stageInfos.substages.map((substage) => H.li(_formatStageInfos(substage)))),
      );
    }
    lis.push(...children);
  }
  return H.ul(...lis);
}
